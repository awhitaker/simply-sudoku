import 'dart:convert';
import 'dart:math';
import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:sudoku/animate_text.dart';
import 'package:sudoku/colors.dart';
import 'package:sudoku/sudoku.dart';

class SudokuBoardRoute extends StatefulWidget {
  @override
  SudokuBoardState createState() => SudokuBoardState();
}

class SudokuBoardState extends State<SudokuBoardRoute> with WidgetsBindingObserver {
  static final List<int> difficultyScale = const [75, 65, 54, 40, 33, 26, 17];
  static final List<String> difficultyLabels = const ["Laughable", "Beginner", "Easy", "Normal", "Hard", "Very Hard", "Insane!"];

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final Stopwatch stopwatch = Stopwatch();
  RestartableTimer timer;
  int selected;
  String boardData;
  String elapsedTime = "";
  bool notesMode;
  bool paused;
  bool revealErrors;
  int historyIndex;
  List<String> history = List<String>();
  Map<String, List<String>> notes = Map<String, List<String>>();
  String clickAssign;
  List<String> solutions;
  int difficulty = difficultyScale[3];

  String generateBoard() {
    return generate(Random().nextInt(2^62), difficulty);
  }

  SudokuBoardState() {
    timer = RestartableTimer(Duration(seconds: 1), updateStopwatch);
    resetState();
  }

  void updateStopwatch() {
    if (stopwatch.isRunning) {
      setState(() {
        Duration duration = stopwatch.elapsed;

        if (duration.inHours > 0) {
          elapsedTime = "¯\\_(ツ)_/¯";
        } else {
          int m = duration.inMinutes;
          int s = duration.inSeconds % 60;
          elapsedTime = m > 0 ? "${m}m ${s}s" : "${s}s";
        }
      });
    }
    timer.reset();
  }

  void resetState() {
    boardData = generateBoard();
    history.clear();
    history.add(boardData);
    history.add(json.encode(notes));
    historyIndex = history.length;
    clickAssign = "";
    selected = -1;
    notes.clear();
    notesMode = false;
    paused = false;
    revealErrors = false;
    stopwatch.reset();
    stopwatch.start();
  }

  void newGame() {
    setState(resetState);
  }

  bool isStatic(index) {
    return index >= 0 && history[0][index] != '.';
  }

  bool isCrossHair(index) {
    return selected > -1 && (index % 9 == selected % 9 || index ~/ 9 == selected ~/ 9);
  }

  void updateSolutions() {
    print("updateSolutions");
    List<String> potentiallySolved = solve(boardData);
    if (potentiallySolved != null) {
      solutions = potentiallySolved;
    }
  }

  bool isSolved() {
    return !boardData.contains('.') && solve(boardData) != null;
  }

  bool isMistake(index) {
    if (!revealErrors || boardData[index] == '.') {
      return false;
    }

    return !solutions[index].contains(boardData[index]);
  }

  void _setSelected(newSelected) {
    if (paused) {
      return;
    }

    // if (isStatic(newSelected) && clickAssign != "") {
    //   return;
    // }

    setState(() {
      selected = isStatic(selected) && newSelected >= 0 && boardData[selected] == clickAssign ? selected : newSelected;
      clickAssign = boardData[selected] == "." || boardData[selected] == clickAssign ? clickAssign : "";
    });

    if (clickAssign != "") {
      _setCell(clickAssign);
    }
  }

  void togglePaused() {
    setState(() {
      paused = !paused;

      if (paused) {
        stopwatch.stop();
      } else {
        stopwatch.start();
      }
    });
  }

  void toggleRevealErrors() {
    setState(() {
      revealErrors = !revealErrors;
    });
  }

  void reset() { setHistoryIndex(2); }
  void undo() { setHistoryIndex(historyIndex - 2); }
  void redo() { setHistoryIndex(historyIndex + 2); }

  void reveal() {
    updateSolutions();
    List<String> options = solutions[selected].split('');
    options.shuffle();
    _setCell(options[0]);
  }

  void setHistoryIndex(int value) {
    if (paused) {
      return;
    }

    setState(() {
      historyIndex = value;
      Map<String, dynamic> deserialized = json.decode(history[historyIndex - 1]);
      notes = deserialized.map((k, v) => MapEntry(k, (v as List).map<String>((list) => list as String).toList()));
      boardData = history[historyIndex - 2];
    });
  }

  void setClickAssign(number) {
    setState(() {
      clickAssign = clickAssign == number ? "" : number;
      selected = selected >= 0 && boardData[selected] != number ? selected : selected;
    });
  }

  void _setCell(number) {
    if (paused || selected < 0) {
      return;
    }

    if (isStatic(selected)) {
      return;
    }

    setState(() {
      if (notesMode && number != "X") {
        if (!notes.containsKey(number)) {
          notes[number] = List<String>();
        }

        List<String> note = notes[number];
        if (note.contains(number)) {
          note.add(number);
          note.sort((a, b) => a.compareTo(b));
        } else {
          note.remove(number);
        }
      } else {
        notes.remove(number);

        String proposedBoardData = 
          boardData.substring(0, selected) + 
          (number == 'X' ? '.' : number) +
          boardData.substring(selected + 1);

        if (proposedBoardData != boardData) {
          if (historyIndex != history.length) {
            history = history.sublist(0, historyIndex);
          }

          boardData = proposedBoardData;
          history.add(boardData);
          history.add(json.encode(notes));
          historyIndex = history.length;
        }
      }

      updateSolutions();
    });
  }

  void toggleNotesMode() {
    if (paused) {
      return;
    }
    setState(() {
      notesMode = !notesMode;
    });
  }

  Future<bool> confirmNewGame() {
    return showDialog<bool>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Quit?"),
          content: Text("Quit current game?"),
          actions: [
            FlatButton(
              child: Text("No"),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
            ),
            FlatButton(
              child: Text("Yes"),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
            ),
          ],
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    setState(() {
      paused = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    bool staticSelected = isStatic(selected);
    bool noEdit = paused || staticSelected;
    int counter = 0;
    final board = [0,1,2,3,4,5,6,7,8].map((row) => boardData.substring(row * 9, row * 9 + 9));

    return Scaffold(
      key: scaffoldKey,
      backgroundColor: themeColor,
      appBar: AppBar(
        actions: [
          Container(
            padding: EdgeInsets.only(top: 16, right: 16),
            child: Text(elapsedTime + " ", style: TextStyle(color: Colors.white, fontSize: 18))
          )
        ],
        backgroundColor: themeColor,
        elevation: 0,
        title: Text("Simply Sudoku"),
        centerTitle: true,
      ),
      body: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              // Text("Difficulty", style: TextStyle(color: Colors.white54, fontSize: 20)),
              FlatButton.icon(
                label: Text("New Game", style: TextStyle(color: Colors.white, fontSize: 18)),
                icon: Icon(Icons.grid_on, color: Colors.white),
                onPressed: () async {
                  if (history.length == 2 || await confirmNewGame()) {
                    newGame();
                  }
                },
              ),
              DropdownButtonHideUnderline(
                child: DropdownButton(
                  underline: null,
                  value: difficulty,
                  hint: Text("Difficulty"),
                  iconEnabledColor: Colors.white70,
                  items: difficultyScale.asMap().entries.map((kv) {
                    return DropdownMenuItem(
                      value: kv.value, 
                      child: Text(difficultyLabels[kv.key], style: TextStyle(color: Colors.white))
                    );
                  }).toList(),
                  onChanged: (value) async {
                    if (history.length == 2 || await confirmNewGame()) {
                      setState(() {
                        difficulty = value;
                      });
                      newGame();
                    }
                  },
                )
              )
            ]
          ),
          Stack(
            children: [
              Opacity(
                opacity: paused ? 0.6 : 1,
                child: Container(
                  padding: const EdgeInsets.all(20.0),
                  child: Table(
                    children: board.map((row) => TableRow(
                      children: row.split('').map((number) {
                        int index = counter;
                        int column = index % 9;
                        int row = index ~/ 9;
                        int column3 = column ~/ 3;
                        int row3 = row ~/ 3;
                        bool rightBorder = ![2,5,8].contains(column);
                        bool bottomBorder = ![2,5,8].contains(row);
                        Color backgroundColor = ((column3 + row3) % 2 == 0) ? themeColor : accentColor;
                        Color borderColor = ((column3 + row3) % 2 == 0) ? accentColor : alternateAccentColor;
                        bool staticCell = isStatic(index);
                        bool highlightCell = number != '.' && (number == clickAssign || (selected > -1 && boardData[selected] == number));
                        bool crossHairCell = isCrossHair(index);
                        bool mistakenCell = isMistake(index);
                        Color baseColorCell = crossHairCell ? backgroundColor.withGreen(backgroundColor.green + 10).withAlpha(200) : backgroundColor;
                        counter++;

                        return TableCell(
                          child: GestureDetector(
                            onTap: () { _setSelected(index); },
                            child: Container (
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom: bottomBorder ? BorderSide(width: 1.0, color: borderColor) : BorderSide.none,
                                  right: rightBorder ? BorderSide(width: 1.0, color: borderColor) : BorderSide.none
                                ),
                                color: mistakenCell ? baseColorCell.withRed(100) : baseColorCell,
                              ),
                              child: Opacity(
                                opacity: paused ? 0.0 : 1.0,
                                child: Container(
                                  padding: const EdgeInsets.all(6.0),
                                  decoration: BoxDecoration(
                                    color: paused ? Colors.transparent : staticCell ? Color(0xC0102935) : index == selected ? (mistakenCell ? selectedColor.withRed(150) : selectedColor.withAlpha(255)) : Colors.transparent,
                                    borderRadius: BorderRadius.all(Radius.circular(index == selected ? 5 : 20.0)),
                                    border: Border.all(width: 1, color: highlightCell ? Colors.yellow.shade400 : Colors.transparent),
                                  ),
                                  child: Center(
                                    child: AnimateText(number == "." ? "" : number, duration: Duration(milliseconds: 200))
                                  )
                                )
                              )
                            )
                          )
                        );
                      }).toList()
                    )).toList()
                  )
                )
              ),
              Positioned(
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                child: Container(
                  child: Center(
                    child: Text(isSolved() ? "Solved!" : "Paused", style: TextStyle(fontSize: 50, color: Colors.white))
                )
                )
              )
            ].sublist(0, paused || isSolved() ? 2 : 1)
          ),
          Spacer(),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 5),
            child: Opacity(
              opacity: noEdit  ? 0.7 : 1.0,
              child: Table(
                children: ["12345","6789X"].map((rows) => (
                  TableRow(
                    children: rows.split('').map((number) {
                      RegExp expression = RegExp("[^$number]*$number");
                      int count = expression.allMatches(boardData).length;
                      bool disabled = noEdit || count == 9;
                      return TableCell(
                        child: Container(
                          padding: EdgeInsets.all(3.0),
                          child: Material(
                            clipBehavior: Clip.antiAlias,
                            borderRadius: BorderRadius.all(Radius.circular(30.0)),
                            color: disabled ? accentColor : alternateAccentColor,
                            child: InkWell(
                              onLongPress: disabled ? null : () { setClickAssign(number); },
                              onTap: disabled ? null : () { setClickAssign(''); _setCell(number); },
                                child: Container (
                                  padding: EdgeInsets.all(number == "X" ? 8.0 : 5.0),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(Radius.circular(30.0)),
                                    border: Border.all(
                                      color: clickAssign == number ? Colors.yellow.shade400 : Colors.transparent
                                    )
                                  ),
                                  child: Center(
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        number == 'X' ? Icon(Icons.block, size: 32, color: disabled ? Colors.white10 : Colors.white) : null,
                                        number == 'X' ? null : Text(number, style: TextStyle(fontSize: 32, color: disabled ? Colors.white10 : Colors.white)),
                                        Text(number == 'X' ? 'Erase' : count.toString(), style: TextStyle(fontSize: 12, color: disabled ? Colors.white10 : Colors.white30)),
                                      ].where((t) => t != null).toList()
                                    )
                                  )
                                )
                              )
                            )
                          )
                      );
                    }).toList()
                  )
                )).toList()
              )
            )
          ),
          Spacer(),
          ButtonBar(
            alignment: MainAxisAlignment.spaceEvenly, 
            children: <Widget>[
              Column(
                children: [
                  Text('Restart', style: TextStyle(fontSize: 12, color: paused || historyIndex == 2 ? Colors.black54 : Colors.white30)),
                  IconButton(
                    tooltip: "Restart Game",
                    icon: Icon(Icons.history),
                    color: Colors.white,
                    onPressed: paused || historyIndex == 2 ? null : reset
                  ),
                ]
              ),

              Column(
                children: [
                  Text('Undo', style: TextStyle(fontSize: 12, color: paused || historyIndex == 2 ? Colors.black54 : Colors.white30)),
                  IconButton(
                    tooltip: "Undo",
                    icon: Icon(Icons.undo),
                    color: Colors.white,
                    onPressed: paused || historyIndex == 2 ? null : undo
                  ),
                ]
              ),

              Column(
                children: [
                  Text('Redo', style: TextStyle(fontSize: 12, color: paused || historyIndex == history.length ? Colors.black54 : Colors.white30)),
                  IconButton(
                    tooltip: "Redo",
                    icon: Icon(Icons.redo),
                    color: Colors.white,
                    onPressed: paused || historyIndex == history.length ? null : redo
                  ),
                ]
              ),

              // Column(
              //   children: [
              //     Text('Notes', style: TextStyle(fontSize: 12, color: paused ? Colors.black54 : notesMode ? Colors.white : Colors.white30)),
              //     Container(
              //       decoration: BoxDecoration(
              //         color: notesMode && !paused ? accentColor : Colors.transparent,
              //         borderRadius: BorderRadius.all(Radius.circular(40.0))
              //       ),
              //       child: IconButton(
              //         icon: Icon(notesMode ? Icons.create : Icons.create,
              //           color: paused ? null : notesMode ? Colors.white : Colors.white30
              //         ),
              //         onPressed: paused ? null : toggleNotesMode
              //       ),
              //     )
              //   ]
              // ),

              // Column(
              //   children: [
              //     Text('Solve', style: TextStyle(fontSize: 12, color: paused ? Colors.black54 : Colors.white30)),
              //     IconButton(
              //       icon: Icon(Icons.check_circle_outline),
              //       color: Colors.white,
              //       onPressed: paused || isSolved() ? null : goSolve
              //     ),
              //   ]
              // ),

              Column(
                children: [
                  Text('Reveal', style: TextStyle(fontSize: 12, color: paused || staticSelected || selected < 0 ? Colors.black54 : Colors.white30)),
                  IconButton(
                    tooltip: "Reveal Current Cell",
                    icon: Icon(Icons.visibility),
                    color: Colors.white,
                    onPressed: paused || staticSelected || selected < 0  ? null : reveal
                  ),
                ]
              ),

              Column(
                children: [
                  Text('Mistakes', style: TextStyle(fontSize: 12, color: paused ? Colors.black54 : revealErrors ? Colors.white : Colors.white30)),
                  Container(
                    decoration: BoxDecoration(
                      color: revealErrors ? accentColor : Colors.transparent,
                      borderRadius: BorderRadius.all(Radius.circular(40.0))
                    ),
                    child: IconButton(
                      tooltip: "Highlight Mistakes",
                      icon: Icon(Icons.warning,
                        color: !paused && revealErrors ? Colors.white : Colors.white30
                      ),
                      onPressed: paused ? null : () { 
                        toggleRevealErrors();
                        // scaffoldKey.currentState.removeCurrentSnackBar();
                        // scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("No errors!")));
                      }
                    ),
                  )
                ]
              ),

              Column(
                children: [
                  Text(paused ? 'Play' : 'Pause', style: TextStyle(fontSize: 12, color: paused ? Colors.white : Colors.white30)),
                  Container(
                    decoration: BoxDecoration(
                      color: paused ? accentColor : Colors.transparent,
                      borderRadius: BorderRadius.all(Radius.circular(40.0))
                    ),
                    child: IconButton(
                      tooltip: "Pause Game",
                      icon: Icon(paused ? Icons.play_circle_outline : Icons.pause, 
                        color: paused ? Colors.white : Colors.white30
                      ),
                      onPressed: togglePaused
                    ),
                  )
                ]
              ),
            ]
          )
        ]
      )
    );
  }
}