import 'package:flutter/material.dart';

Color themeColor = Color(0xff01384A);
Color accentColor = Color(0xff114556);
Color alternateAccentColor = Color(0xff195062);
Color selectedColor = Color(0x20195253);

// Color themeColor = Color(0xff014A38);
// Color accentColor = Color(0xff115645);
// Color alternateAccentColor = Color(0xff196250);
// Color selectedColor = Color(0x20195352);

// Color themeColor = Color(0xff38014A);
// Color accentColor = Color(0xff451156);
// Color alternateAccentColor = Color(0xff501962);
// Color selectedColor = Color(0x20521953);

// Color themeColor = Color(0xff384A4A);
// Color accentColor = Color(0xff455656);
// Color alternateAccentColor = Color(0xff506262);
// Color selectedColor = Color(0x20525353);

// Color themeColor = Color(0xff380101);
// Color accentColor = Color(0xff451111);
// Color alternateAccentColor = Color(0xff501919);
// Color selectedColor = Color(0x20521919);
