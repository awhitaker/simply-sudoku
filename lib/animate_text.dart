import 'package:flutter/material.dart';

class AnimateText extends StatefulWidget {
  AnimateText(this.text, {this.maxScale, this.duration});
  final String text;
  final double maxScale;
  final Duration duration;

  @override
  createState() => new AnimateTextState();
}

class AnimateTextState extends State<AnimateText>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  double textScale = 1.0;
  String widgetText;

  initState() {
    super.initState();
    controller = new AnimationController(duration: widget.duration, vsync: this);
    final CurvedAnimation curve =
        new CurvedAnimation(parent: controller, curve: Curves.easeInOut);
    final animation = new Tween<double>(begin: 0.0, end: 1.0).animate(curve)
      ..addStatusListener((status) {
        // if (status == AnimationStatus.completed)
        //   controller.reverse();
        // if (status == AnimationStatus.dismissed) controller.forward();
      });
    animation.addListener(() => setState(() => textScale = animation.value));
    controller.forward();
  }

  @override
  dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.text != widgetText) {
      if (controller.isAnimating) {
        controller.stop(canceled: true);
      }
      controller.value = 0.0;
      controller.forward();
      widgetText = widget.text;
    }

    return Opacity(
      opacity: textScale,
      child: Text(
        widget.text,
        style: TextStyle(color: Colors.white, fontSize: 20),
        // textScaleFactor: textScale,
      )
    );
  }
}