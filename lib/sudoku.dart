library sudoku; 
import 'dart:math' as math;

const rows = 'ABCDEFGHI';
const cols = '123456789';
final rowList = rows.split('');   // ['A', 'B', ...]
final colList = cols.split('');   // ['1', '2', ...]

// squares contains all cells in "RC" format where R is a letter from 'rows' and C is a number from cols;
// squares = ['A1', 'A2', ... 'I8', 'I9'];
final squares = cross(rows, cols);

final unitlist = colList.map((c) => cross(rows, c)).toList()
  ..addAll(rowList.map((r) => cross(r, cols)))
  ..addAll(['ABC','DEF','GHI'].expand((rs) => ['123','456','789'].map((cs) => cross(rs, cs) )));

final units = toMap(squares, (s) => unitlist.where((u) => u.contains(s)).toList());

final peers = toMap(squares, (s) => units[s].expand((u) => u).where((u) => u != s).toList());

// Cross Join
// Example: cross('ab', '12') => ['a1', 'a2', 'b1', 'b2']
List<String> cross(String left, String right) {
  return left.split('').expand((l) => right.split('').map((r) => l + r)).toList();
} 

// Create a Map from an iteratble and a value provider func
Map<T, T2> toMap<T, T2>(Iterable<T> list, T2 provider(T)) {
  return Map.fromEntries(list.map((i) => MapEntry(i, provider(i))));
}

Map<String, String> parseGrid(String grid) {
  var values = toMap(squares, (s) => cols);
  var gridVals = gridValues(grid);
  
  for (var s in gridVals.keys){
    var d = gridVals[s];
    if (cols.contains(d) && assign(values, s, d) == null) {
      return null;
    }
  }
  return values;
}

Map gridValues(String grid) {
  var chars = grid.split('').where((c) => cols.contains(c) || c == '.').toList();
  return squares.asMap().map((index, value) => MapEntry(value, chars[index]));
}

Map assign(Map values, String s, String d) {
  var otherValues = values[s].replaceAll(d, '');
  if (otherValues.split('').map((d2) => eliminate(values, s, d2)).every((e) => e != null)) {
    return values;
  }
  return null;
}

Map eliminate(Map values, String s, String d){
  if (!values[s].contains(d)) {
    return values;
  }
  
  values[s] = values[s].replaceAll(d,'');
  
  if (values[s].length == 0) {
    return null;
  }
  
  if (values[s].length == 1) {
    var d2 = values[s];

    if (!peers[s].map((s2) => eliminate(values, s2, d2)).every((e) => e != null)) {
      return null;
    }
  }

  for (var u in units[s]){
    var dplaces = u.where((s) => values[s].contains(d));
    var length = dplaces.length;
    if (length == 0 || (length == 1 && (assign(values, dplaces.elementAt(0), d) == null))) {
      return null;
    }
  }
  return values;
}

List<String> solve(String data){
  var values = parseGrid(data);
  return (values == null) ? null : rowList.map((r) => colList.map((c) => values[r + c])).expand((u) => u).toList();
}

String generate(seed, count) {
  var random = math.Random(seed);
  var board = '.................................................................................';
  var solutions = solve(board);
  var iterations = 90;
  var localCount = count;

  while (localCount > 0 && iterations-- > 0) {
    var empty = board.split('').asMap().entries.where((kv) => kv.value == '.').map((kv) => kv.key).toList();
    var index = empty[random.nextInt(empty.length)];
    var options = solutions[index];
    var number = options[random.nextInt(options.length)].toString();
    var nextBoard = board.substring(0, index) + number + board.substring(index + 1);
    var nextSolutions = solve(nextBoard);

    // print(board);
    // print(nextBoard);
    // print((nextSolutions != null ? "SUCCESS" : "FAILURE: ") + " - [${index.toString()}] = $number;   // $options");
    // display(parseGrid(board));

    if (nextSolutions != null) {
      board = nextBoard;
      solutions = nextSolutions;
      localCount--;
    }
  }

  if (localCount > 0) {
    return generate(seed + 1, count);
  }

  return board;
}

//---------------------------------------------------------------------------------------
// Utilities and Helpers - not needed in production
//
String repeat(String s, int n) {
  var sb = new StringBuffer();
  for (var i = 0; i < n; i++) {
    sb.write(s);
  }
  return sb.toString();
}

String center(String s, int max, [String pad=" "]) {
  var padLen = max - s.length;
  if (padLen <= 0) {
    return s;
  }
  s = repeat(pad, (padLen ~/ 2)) + s;
  return s + repeat(pad, max - s.length);
}

void display(Map values) {
  var width = 1 + squares.map((s) => values[s].length as num).reduce((a, b) => math.max(a, b));
  var line = repeat('+' + repeat('-', width*3), 3).substring(1);  

  print("");
  print("");
  rowList.forEach((r){
    print(colList.map((c) => center(values[r+c], width) + ('36'.contains(c) ? '|' : '')).toList().join(''));

    if ('CF'.contains(r)) {
      print(line);
    }
  });
  print("");
}