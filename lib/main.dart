import 'package:flutter/material.dart';
import 'package:sudoku/colors.dart';
import 'package:sudoku/sudoku_board.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Simply Sudoku',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        canvasColor: themeColor.withAlpha(240)
      ),
      home: SudokuBoardRoute(),
    );
  }
}
